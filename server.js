const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');
const LocalStrategy = require('passport-local').Strategy;


app.use(bodyParser.urlencoded({ extended: false })) 
app.use(bodyParser.json())

require('dotenv').config()

const connection = mysql.createConnection({
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    database : process.env.DB_DATABASE
  });

connection.connect((err) => {
    if(err){
        console.log(err);
        return;
    }
    console.log("server Ok!")
});

app.set('view engine', 'pug');

app.use(express.static(`${__dirname}/static`))

app.get('/', (req, res) => res.render('post/connect'))

app.post('/post/inscription', (req, res) => {
    console.log(req.body);
    connection.query(`INSERT INTO users (login, mail, password) VALUES ('${req.body.login}', '${req.body.mail}', '${req.body.mdp}');`, (err, result,fields) => {
        if(err){
            console.log(err);
        }
    });
    res.redirect('/layout');
})


app.listen(process.env.PORT, () => console.log(`listen on ${process.env.PORT}`))